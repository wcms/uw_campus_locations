// Set up variables so they persist while looping through the function.
(function ($) {
  var prev_name = '';
  var prev_item;
  $(document).ready(function () {
    $('.views-row', '.view-uw-campus-locations').each(function (index) {
      // Find the name of the location and add it as a data attribute.
      var name = $('.views-field-title span', this).html();
      console.log(name);
      $(this).attr('data-name', name);
      // Do things if this name is the same as the one before, meaning this is a duplicate.
      if (name == prev_name) {
        // If the previous item is visible, it was the first item.
        if ($(prev_item).filter(":visible").length) {
          // Create a copy of the first item that we will modify and insert.
          var trigger_item = $(prev_item).clone();
          $(trigger_item).addClass('trigger');
          $('.views-field-field-outlet-location .field-content', trigger_item).html('<div class="multiple-locations">Multiple locations<br><hr><button title="show all">Show all locations</button></div>');
          $(trigger_item).insertBefore(prev_item);
          $('.outlet-description', trigger_item).hide();
          $('.views-field-nid', trigger_item).hide();
          // Hide the first item.
          $(prev_item).hide();
        }
        // Hide this duplicate item.
        $(this).hide();
      }
      // Update the values for the next loop.
      else {
        prev_name = name;
        prev_item = this;
      }
    });
    $('.view-uw-campus-locations .trigger button').on('click', function () {
      var current_state = $(this).html();
      if (current_state == 'Show all locations') {
        // Update the button to indicate the action has changed.
        $(this).html('Hide Locations');
        $(this).attr('title', 'hide all');
        // Show everything with this name.
        var item_name = $(this).closest('.trigger').attr('data-name');
        $('.views-row[data-name="' + item_name + '"]:not(.trigger)', '.view-uw-campus-locations')
          .show()
          .css({marginLeft: "-100px", marginRight: "100px", opacity: "0", clipPath: "inset(0 0 0 100px)"})
          .animate({marginLeft: "0", marginRight: "0", opacity: "1", clipStart: 100},
            {
              duration: 300,
              step: function (now, fx) { //now is the animated value from initial css value
                if (fx.prop == 'clipStart') {
                  pixels = 100 - now;
                  $(this).css({clipPath: "inset(0 0 0 " + pixels + "px)"});
                }
              }
            });
      }
      else {
        // Update the button to indicate the action has changed.
        $(this).html('Show all locations');
        $(this).attr('title', 'show all');
        // Hide everything with this name.
        var item_name = $(this).closest('.trigger').attr('data-name');
        $('.views-row[data-name="' + item_name + '"]:not(.trigger)', '.view-uw-campus-locations')
          .animate({marginLeft: "-100px", marginRight: "100px", opacity: "0", clipStart: 0},
            {
              duration: 300,
              step: function (now, fx) { //now is the animated value from initial css value
                if (fx.prop == 'clipStart') {
                  pixels = 100 - now;
                  $(this).css({clipPath: "inset(0 0 0 " + pixels + "px)"});
                }
              },
              complete: function () {
                $(this).hide();
              }
            }
          );
      }
    });
  });
}(jQuery));
