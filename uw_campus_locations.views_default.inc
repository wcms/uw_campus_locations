<?php

/**
 * @file
 * uw_campus_locations.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function uw_campus_locations_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'uw_campus_locations';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'On-Campus Locations';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'On-campus locations';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Submit';
  $handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'title' => 'title',
  );
  /* No results behavior: Global: Unfiltered text */
  $handler->display->display_options['empty']['area_text_custom']['id'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['table'] = 'views';
  $handler->display->display_options['empty']['area_text_custom']['field'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['empty'] = TRUE;
  $handler->display->display_options['empty']['area_text_custom']['content'] = 'No results matching your criteria. Please click reset to try a different search. ';
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  /* Field: Content: Photo of Outlet  */
  $handler->display->display_options['fields']['field_outlet_photo']['id'] = 'field_outlet_photo';
  $handler->display->display_options['fields']['field_outlet_photo']['table'] = 'field_data_field_outlet_photo';
  $handler->display->display_options['fields']['field_outlet_photo']['field'] = 'field_outlet_photo';
  $handler->display->display_options['fields']['field_outlet_photo']['label'] = '';
  $handler->display->display_options['fields']['field_outlet_photo']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_outlet_photo']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_outlet_photo']['settings'] = array(
    'image_style' => 'uw_food_outlet_image_style',
    'image_link' => 'content',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Description */
  $handler->display->display_options['fields']['field_description']['id'] = 'field_description';
  $handler->display->display_options['fields']['field_description']['table'] = 'field_data_field_description';
  $handler->display->display_options['fields']['field_description']['field'] = 'field_description';
  $handler->display->display_options['fields']['field_description']['label'] = '';
  $handler->display->display_options['fields']['field_description']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_description']['element_wrapper_class'] = 'outlet-description';
  /* Field: Content: Hours of Operation */
  $handler->display->display_options['fields']['field_opening_hours']['id'] = 'field_opening_hours';
  $handler->display->display_options['fields']['field_opening_hours']['table'] = 'field_data_field_opening_hours';
  $handler->display->display_options['fields']['field_opening_hours']['field'] = 'field_opening_hours';
  $handler->display->display_options['fields']['field_opening_hours']['label'] = '';
  $handler->display->display_options['fields']['field_opening_hours']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_opening_hours']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_opening_hours']['alter']['text'] = '<li>
<a href="hours-of-operation#[nid]"><img src="sites/d7.fdsu4/modules/uw_campus_locations/images/hours_icon.png" ></a>

</li>';
  $handler->display->display_options['fields']['field_opening_hours']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_opening_hours']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_opening_hours']['click_sort_column'] = 'day';
  $handler->display->display_options['fields']['field_opening_hours']['settings'] = array(
    'showclosed' => 'all',
    'date_first_day' => '0',
    'daysformat' => 'long',
    'hoursformat' => '0',
    'compress' => 0,
    'grouped' => 0,
    'closedformat' => 'Closed',
    'separator_days' => '<br />',
    'separator_grouped_days' => ' - ',
    'separator_day_hours' => ': ',
    'separator_hours_hours' => '-',
    'separator_more_hours' => ', ',
    'current_status' => array(
      'position' => 'hide',
      'open_text' => 'Currently open!',
      'closed_text' => 'Currently closed',
    ),
  );
  $handler->display->display_options['fields']['field_opening_hours']['delta_offset'] = '0';
  /* Field: Content: Location */
  $handler->display->display_options['fields']['field_outlet_location']['id'] = 'field_outlet_location';
  $handler->display->display_options['fields']['field_outlet_location']['table'] = 'field_data_field_outlet_location';
  $handler->display->display_options['fields']['field_outlet_location']['field'] = 'field_outlet_location';
  $handler->display->display_options['fields']['field_outlet_location']['label'] = '';
  $handler->display->display_options['fields']['field_outlet_location']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_outlet_location']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_outlet_location']['alter']['text'] = '<li>

<a href="map"><img src="sites/d7.fdsu4/modules/uw_campus_locations/images/location_icon.png" ></a>

</li>';
  $handler->display->display_options['fields']['field_outlet_location']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_outlet_location']['hide_empty'] = TRUE;
  /* Field: Content: Franchise Menus */
  $handler->display->display_options['fields']['field_uw_fo_franchise_menus']['id'] = 'field_uw_fo_franchise_menus';
  $handler->display->display_options['fields']['field_uw_fo_franchise_menus']['table'] = 'field_data_field_uw_fo_franchise_menus';
  $handler->display->display_options['fields']['field_uw_fo_franchise_menus']['field'] = 'field_uw_fo_franchise_menus';
  $handler->display->display_options['fields']['field_uw_fo_franchise_menus']['label'] = '';
  $handler->display->display_options['fields']['field_uw_fo_franchise_menus']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_uw_fo_franchise_menus']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_uw_fo_franchise_menus']['alter']['text'] = '<li>
<a href="uw_ct_franchise"><img src="sites/d7.fdsu4/modules/uw_campus_locations/images/menu_icon.png" ></a>

</li>';
  $handler->display->display_options['fields']['field_uw_fo_franchise_menus']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_uw_fo_franchise_menus']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_uw_fo_franchise_menus']['settings'] = array(
    'bypass_access' => 0,
    'link' => 0,
  );
  $handler->display->display_options['fields']['field_uw_fo_franchise_menus']['delta_offset'] = '0';
  /* Field: Broken/missing handler */
  $handler->display->display_options['fields']['field_outlet_menu']['id'] = 'field_outlet_menu';
  $handler->display->display_options['fields']['field_outlet_menu']['table'] = 'field_data_field_outlet_menu';
  $handler->display->display_options['fields']['field_outlet_menu']['field'] = 'field_outlet_menu';
  $handler->display->display_options['fields']['field_outlet_menu']['label'] = '';
  $handler->display->display_options['fields']['field_outlet_menu']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_outlet_menu']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_outlet_menu']['alter']['text'] = '<li>

<a href="uw_food_outlet#[field_outlet_id]"><img src="sites/d7.fdsu4/modules/uw_campus_locations/images/menu_icon.png" ></a>

</li>';
  $handler->display->display_options['fields']['field_outlet_menu']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_outlet_menu']['hide_empty'] = TRUE;
  /* Field: Broken/missing handler */
  $handler->display->display_options['fields']['field_dm_outlet_station']['id'] = 'field_dm_outlet_station';
  $handler->display->display_options['fields']['field_dm_outlet_station']['table'] = 'field_data_field_dm_outlet_station';
  $handler->display->display_options['fields']['field_dm_outlet_station']['field'] = 'field_dm_outlet_station';
  $handler->display->display_options['fields']['field_dm_outlet_station']['ui_name'] = 'Content: Food Station Daily Menu';
  $handler->display->display_options['fields']['field_dm_outlet_station']['label'] = '';
  $handler->display->display_options['fields']['field_dm_outlet_station']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_dm_outlet_station']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_dm_outlet_station']['alter']['text'] = '<li>
<a href="/food-services/menu"><img src="sites/d7.fdsu4/modules/uw_campus_locations/images/menu_icon.png" ></a>
<!--should really be uw_fs_daily_menu -->

</li>';
  $handler->display->display_options['fields']['field_dm_outlet_station']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_dm_outlet_station']['hide_empty'] = TRUE;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<ul class="locationsIcon">
[field_opening_hours]
[field_outlet_location]
[field_uw_fo_franchise_menus]
[field_outlet_menu]
[field_dm_outlet_station]
</ul>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  /* Field: File: Path */
  $handler->display->display_options['fields']['uri']['id'] = 'uri';
  $handler->display->display_options['fields']['uri']['table'] = 'file_managed';
  $handler->display->display_options['fields']['uri']['field'] = 'uri';
  $handler->display->display_options['fields']['uri']['relationship'] = 'node_to_file';
  /* Sort criterion: Content: Sticky status */
  $handler->display->display_options['sorts']['sticky']['id'] = 'sticky';
  $handler->display->display_options['sorts']['sticky']['table'] = 'node';
  $handler->display->display_options['sorts']['sticky']['field'] = 'sticky';
  $handler->display->display_options['sorts']['sticky']['order'] = 'DESC';
  /* Sort criterion: Content: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  /* Sort criterion: Content: Location (field_outlet_location) */
  $handler->display->display_options['sorts']['field_outlet_location_value']['id'] = 'field_outlet_location_value';
  $handler->display->display_options['sorts']['field_outlet_location_value']['table'] = 'field_data_field_outlet_location';
  $handler->display->display_options['sorts']['field_outlet_location_value']['field'] = 'field_outlet_location_value';
  /* Filter criterion: Content: Published status */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'uw_food_outlet' => 'uw_food_outlet',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Location (field_outlet_location) */
  $handler->display->display_options['filters']['field_outlet_location_value']['id'] = 'field_outlet_location_value';
  $handler->display->display_options['filters']['field_outlet_location_value']['table'] = 'field_data_field_outlet_location';
  $handler->display->display_options['filters']['field_outlet_location_value']['field'] = 'field_outlet_location_value';
  $handler->display->display_options['filters']['field_outlet_location_value']['operator'] = 'word';
  $handler->display->display_options['filters']['field_outlet_location_value']['group'] = 1;
  $handler->display->display_options['filters']['field_outlet_location_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_outlet_location_value']['expose']['operator_id'] = 'field_outlet_location_value_op';
  $handler->display->display_options['filters']['field_outlet_location_value']['expose']['label'] = 'Location';
  $handler->display->display_options['filters']['field_outlet_location_value']['expose']['operator'] = 'field_outlet_location_value_op';
  $handler->display->display_options['filters']['field_outlet_location_value']['expose']['identifier'] = 'field_outlet_location_value';
  $handler->display->display_options['filters']['field_outlet_location_value']['expose']['remember_roles'] = array(
    2 => '2',
  );
  $handler->display->display_options['filters']['field_outlet_location_value']['group_info']['label'] = 'Location (field_outlet_location)';
  $handler->display->display_options['filters']['field_outlet_location_value']['group_info']['identifier'] = 'field_outlet_location_value';
  $handler->display->display_options['filters']['field_outlet_location_value']['group_info']['remember'] = FALSE;
  $handler->display->display_options['filters']['field_outlet_location_value']['group_info']['group_items'] = array(
    1 => array(),
    2 => array(),
    3 => array(),
  );
  /* Filter criterion: Content: Outlet Type (field_outlet_type) */
  $handler->display->display_options['filters']['field_outlet_type_tid']['id'] = 'field_outlet_type_tid';
  $handler->display->display_options['filters']['field_outlet_type_tid']['table'] = 'field_data_field_outlet_type';
  $handler->display->display_options['filters']['field_outlet_type_tid']['field'] = 'field_outlet_type_tid';
  $handler->display->display_options['filters']['field_outlet_type_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_outlet_type_tid']['expose']['operator_id'] = 'field_outlet_type_tid_op';
  $handler->display->display_options['filters']['field_outlet_type_tid']['expose']['label'] = 'Outlet Type';
  $handler->display->display_options['filters']['field_outlet_type_tid']['expose']['operator'] = 'field_outlet_type_tid_op';
  $handler->display->display_options['filters']['field_outlet_type_tid']['expose']['identifier'] = 'field_outlet_type_tid';
  $handler->display->display_options['filters']['field_outlet_type_tid']['expose']['remember_roles'] = array(
    2 => '2',
  );
  $handler->display->display_options['filters']['field_outlet_type_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_outlet_type_tid']['vocabulary'] = 'uw_food_outlet_types';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  /* Field: Content: Path */
  $handler->display->display_options['fields']['path']['id'] = 'path';
  $handler->display->display_options['fields']['path']['table'] = 'node';
  $handler->display->display_options['fields']['path']['field'] = 'path';
  $handler->display->display_options['fields']['path']['exclude'] = TRUE;
  /* Field: Content: Photo of Outlet  */
  $handler->display->display_options['fields']['field_outlet_photo']['id'] = 'field_outlet_photo';
  $handler->display->display_options['fields']['field_outlet_photo']['table'] = 'field_data_field_outlet_photo';
  $handler->display->display_options['fields']['field_outlet_photo']['field'] = 'field_outlet_photo';
  $handler->display->display_options['fields']['field_outlet_photo']['label'] = '';
  $handler->display->display_options['fields']['field_outlet_photo']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_outlet_photo']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_outlet_photo']['settings'] = array(
    'image_style' => 'uw_campus_locations_food_outlet_',
    'image_link' => 'content',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Content: Location Heading */
  $handler->display->display_options['fields']['field_outlet_location_1']['id'] = 'field_outlet_location_1';
  $handler->display->display_options['fields']['field_outlet_location_1']['table'] = 'field_data_field_outlet_location';
  $handler->display->display_options['fields']['field_outlet_location_1']['field'] = 'field_outlet_location';
  $handler->display->display_options['fields']['field_outlet_location_1']['ui_name'] = 'Content: Location Heading';
  $handler->display->display_options['fields']['field_outlet_location_1']['label'] = '';
  $handler->display->display_options['fields']['field_outlet_location_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_outlet_location_1']['element_wrapper_class'] = 'outlet-location-heading';
  /* Field: Content: Summary */
  $handler->display->display_options['fields']['field_outlet_summary']['id'] = 'field_outlet_summary';
  $handler->display->display_options['fields']['field_outlet_summary']['table'] = 'field_data_field_outlet_summary';
  $handler->display->display_options['fields']['field_outlet_summary']['field'] = 'field_outlet_summary';
  $handler->display->display_options['fields']['field_outlet_summary']['label'] = '';
  $handler->display->display_options['fields']['field_outlet_summary']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_outlet_summary']['element_wrapper_class'] = 'outlet-summary';
  /* Field: Content: Hours of Operation */
  $handler->display->display_options['fields']['field_opening_hours']['id'] = 'field_opening_hours';
  $handler->display->display_options['fields']['field_opening_hours']['table'] = 'field_data_field_opening_hours';
  $handler->display->display_options['fields']['field_opening_hours']['field'] = 'field_opening_hours';
  $handler->display->display_options['fields']['field_opening_hours']['label'] = '';
  $handler->display->display_options['fields']['field_opening_hours']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_opening_hours']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_opening_hours']['alter']['text'] = '<li><a href="hours-of-operation#nid[nid]" aria-label="Hours of operation"><span class="ifdsu fdsu-clock2"></span></a></li>';
  $handler->display->display_options['fields']['field_opening_hours']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_opening_hours']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_opening_hours']['click_sort_column'] = 'day';
  $handler->display->display_options['fields']['field_opening_hours']['settings'] = array(
    'showclosed' => 'all',
    'date_first_day' => '0',
    'daysformat' => 'long',
    'hoursformat' => '0',
    'compress' => 0,
    'grouped' => 0,
    'closedformat' => 'Closed',
    'separator_days' => '<br />',
    'separator_grouped_days' => ' - ',
    'separator_day_hours' => ': ',
    'separator_hours_hours' => '-',
    'separator_more_hours' => ', ',
    'current_status' => array(
      'position' => 'hide',
      'open_text' => 'Currently open!',
      'closed_text' => 'Currently closed',
    ),
  );
  $handler->display->display_options['fields']['field_opening_hours']['delta_offset'] = '0';
  /* Field: Content: Location link */
  $handler->display->display_options['fields']['field_uw_fs_location_link']['id'] = 'field_uw_fs_location_link';
  $handler->display->display_options['fields']['field_uw_fs_location_link']['table'] = 'field_data_field_uw_fs_location_link';
  $handler->display->display_options['fields']['field_uw_fs_location_link']['field'] = 'field_uw_fs_location_link';
  $handler->display->display_options['fields']['field_uw_fs_location_link']['label'] = '';
  $handler->display->display_options['fields']['field_uw_fs_location_link']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_uw_fs_location_link']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_uw_fs_location_link']['alter']['text'] = '<li><a href="[field_uw_fs_location_link]" aria-label="Location"><span class="ifdsu fdsu-location"></span></a></li>';
  $handler->display->display_options['fields']['field_uw_fs_location_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_uw_fs_location_link']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_uw_fs_location_link']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_uw_fs_location_link']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_uw_fs_location_link']['type'] = 'link_plain';
  $handler->display->display_options['fields']['field_uw_fs_location_link']['settings'] = array(
    'custom_title' => '',
  );
  /* Field: Content: Franchise Menus */
  $handler->display->display_options['fields']['field_uw_fo_franchise_menus']['id'] = 'field_uw_fo_franchise_menus';
  $handler->display->display_options['fields']['field_uw_fo_franchise_menus']['table'] = 'field_data_field_uw_fo_franchise_menus';
  $handler->display->display_options['fields']['field_uw_fo_franchise_menus']['field'] = 'field_uw_fo_franchise_menus';
  $handler->display->display_options['fields']['field_uw_fo_franchise_menus']['label'] = '';
  $handler->display->display_options['fields']['field_uw_fo_franchise_menus']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_uw_fo_franchise_menus']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_uw_fo_franchise_menus']['alter']['text'] = '<li><a href="[path]" aria-label="Details"><span class="ifdsu fdsu-spoon-knife"></span></a></li>';
  $handler->display->display_options['fields']['field_uw_fo_franchise_menus']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_uw_fo_franchise_menus']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_uw_fo_franchise_menus']['settings'] = array(
    'bypass_access' => 0,
    'link' => 0,
  );
  $handler->display->display_options['fields']['field_uw_fo_franchise_menus']['group_rows'] = FALSE;
  $handler->display->display_options['fields']['field_uw_fo_franchise_menus']['delta_offset'] = '0';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<ul class="locationsIcon">
[field_opening_hours]
[field_uw_fs_location_link]
[field_uw_fo_franchise_menus]
</ul>';
  $handler->display->display_options['fields']['nothing']['element_type'] = 'div';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $handler->display->display_options['path'] = 'locations-and-hours/on-campus-locations';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'On-campus locations';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $translatables['uw_campus_locations'] = array(
    t('Master'),
    t('On-campus locations'),
    t('more'),
    t('Submit'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('No results matching your criteria. Please click reset to try a different search. '),
    t('<li>
<a href="hours-of-operation#[nid]"><img src="sites/d7.fdsu4/modules/uw_campus_locations/images/hours_icon.png" ></a>

</li>'),
    t('<li>

<a href="map"><img src="sites/d7.fdsu4/modules/uw_campus_locations/images/location_icon.png" ></a>

</li>'),
    t('<li>
<a href="uw_ct_franchise"><img src="sites/d7.fdsu4/modules/uw_campus_locations/images/menu_icon.png" ></a>

</li>'),
    t('<li>

<a href="uw_food_outlet#[field_outlet_id]"><img src="sites/d7.fdsu4/modules/uw_campus_locations/images/menu_icon.png" ></a>

</li>'),
    t('<li>
<a href="/food-services/menu"><img src="sites/d7.fdsu4/modules/uw_campus_locations/images/menu_icon.png" ></a>
<!--should really be uw_fs_daily_menu -->

</li>'),
    t('<ul class="locationsIcon">
[field_opening_hours]
[field_outlet_location]
[field_uw_fo_franchise_menus]
[field_outlet_menu]
[field_dm_outlet_station]
</ul>'),
    t('Path'),
    t('Location'),
    t('Location (field_outlet_location)'),
    t('Outlet Type'),
    t('Page'),
    t('<li><a href="hours-of-operation#nid[nid]" aria-label="Hours of operation"><span class="ifdsu fdsu-clock2"></span></a></li>'),
    t('<li><a href="[field_uw_fs_location_link]" aria-label="Location"><span class="ifdsu fdsu-location"></span></a></li>'),
    t('<li><a href="[path]" aria-label="Details"><span class="ifdsu fdsu-spoon-knife"></span></a></li>'),
    t('<ul class="locationsIcon">
[field_opening_hours]
[field_uw_fs_location_link]
[field_uw_fo_franchise_menus]
</ul>'),
  );
  $export['uw_campus_locations'] = $view;

  return $export;
}
