<?php

/**
 * @file
 * uw_campus_locations.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_campus_locations_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_campus_locations_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function uw_campus_locations_image_default_styles() {
  $styles = array();

  // Exported image style: uw_campus_locations_food_outlet_.
  $styles['uw_campus_locations_food_outlet_'] = array(
    'label' => 'UW campus locations food outlet ',
    'effects' => array(
      1 => array(
        'name' => 'image_resize',
        'data' => array(
          'width' => 200,
          'height' => 145,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
