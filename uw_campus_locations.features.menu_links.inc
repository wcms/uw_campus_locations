<?php

/**
 * @file
 * uw_campus_locations.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_campus_locations_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu:locations-and-hours/on-campus-locations.
  $menu_links['main-menu:locations-and-hours/on-campus-locations'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'locations-and-hours/on-campus-locations',
    'router_path' => 'locations-and-hours/on-campus-locations',
    'link_title' => 'On-campus locations',
    'parent_path' => 'locations-and-hours',
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('On-campus locations');

  return $menu_links;
}
